import { TestBed } from '@angular/core/testing';

import { TokenClienteService } from './token-cliente.service';

describe('TokenClienteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TokenClienteService = TestBed.get(TokenClienteService);
    expect(service).toBeTruthy();
  });
});
