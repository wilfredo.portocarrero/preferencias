import { Component, OnInit, Renderer, ElementRef } from '@angular/core';
import * as $ from 'jquery';
import { PreferenciasService } from '../preferencias.service';

@Component({
  selector: 'app-preferencia',
  templateUrl: './preferencia.component.html',
  styleUrls: ['./preferencia.component.css']
})
export class PreferenciaComponent implements OnInit {

  constructor(protected preferencia: PreferenciasService, private renderer: Renderer, private elem: ElementRef) { }
  preferenciasUsuario: any[] = [];
  seleccion = {};
  array3: any[] = [];

  ngOnInit() {
    //listado de intereses
    this.preferenciasUsuario = JSON.parse(localStorage.getItem('preferenciaUsuario'));
  }

  //funcion para quitar elemento de arreglo
  removeItemFromArr(arr, item) {
    var i = arr.indexOf(item);

    if (i !== -1) {
      arr.splice(i, 1);
    }
  }

  //llena o vacia el arreglo
  llenado(id) {
    if (this.array3.includes(id)) {
      this.removeItemFromArr(this.array3, id);
      console.log(this.array3);
    } else {
      this.array3.push(id);
      console.log(this.array3);
    }
  }

  //envia arreglo al servicio
  enviarInteres() {
    this.enviarPreferencia(this.array3);
  }

  //funcion para usar el servicio
  enviarPreferencia(pref) {
    this.preferencia.envPreferencia(pref).subscribe(
      (data) => {
        console.log('este es tu resultado de envio pref ', data);

      },
      (error) => {
        console.log(error);
      }
    )
  }




















  /*

   id: any;
  isClicked: any = false;
  
   elegirInteres(pref){
      if($("#"+pref).hasClass("bg-success")){
       $("#"+pref).removeClass("seleccionado bg-success text-white");
     }else{
       $("#"+pref).addClass("seleccionado bg-success text-white");
     }
     }
  
  ngAfterViewInit(){
      // you'll get your through 'elements' below code
      let elements = this.elem.nativeElement.querySelectorAll('.seleccionado');
      for(var i=0;i<elements.length;i++){
        console.log(elements);
      }
      console.log(elements);
    }
  
   enviarInteres(){
      
      var array = new Array();
      var array2 = [];
      $(".seleccionado").each(function() {
        var elemento = $(this).attr("id");
        array2.push(elemento);
    });
    array.push ( {"aPreferencias":array2} );
    console.log(array);
    this.enviarPreferencia(this.array3);
    }
  */




}
