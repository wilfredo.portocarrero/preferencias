import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TokenClienteService {

  constructor(protected http: HttpClient) {}


  getClientToken() {

    var headers = {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${localStorage.getItem('token')}`)
    }

    const data = {'sEmail': 'wilfredo.portocarrero@materiagris.pe', 'sPassword': '123456'};
    console.log(localStorage.getItem('token'));
    return this.http.post("https://us-central1-devthepoint-47564.cloudfunctions.net/login-inWithEmail",data, headers);

  }
}
